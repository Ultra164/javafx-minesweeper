package sample.controller;

import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.util.Callback;
import sample.model.*;
import sample.model.Tile;
import sample.model.bomb.SmileBomb;
import sample.model.bomb.StandartBomb;
import sample.view.FieldView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by ultra on 14.07.2016.
 */
public class Game {
    private static final int[][] NEIGHBORS_POINTS = {
            {-1, -1},
            {-1, 0},
            {-1, 1},
            {0, -1},
            {0, 1},
            {1, -1},
            {1, 0},
            {1, 1}
    };

    private final static int WINDOW_WIDTH = 640;
    private final static int WINDOW_HEIGHT = 480;
    private final static int TILE_WIDTH_AMOUNT = 20;
    private final static int TILE_HEIGHT_AMOUNT = 15;

    private FieldView fieldView;
    private sample.model.Tile[][] tiles;

    public Game() {
        initViews();
        startGame();
    }

    private void startGame() {
        calculateTiles();
        fieldView.initView(tiles);
    }

    private void endGame() {
        fieldView.destroy();

        for (int x = 0; x < TILE_WIDTH_AMOUNT; x++) {
            for (int y = 0; y < TILE_HEIGHT_AMOUNT; y++) {
                Tile tile = tiles[x][y];
                tile.removeObserver(tileObserver);

                Bomb bomb = tile.getBomb();
                if (bomb != null)
                    bomb.removeObserver(bombObserver);
            }
        }
    }

    private void initViews() {
        fieldView = new FieldView(WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    private void calculateTiles() {
        tiles = new Tile[TILE_WIDTH_AMOUNT][TILE_HEIGHT_AMOUNT];

        for (int x = 0; x < TILE_WIDTH_AMOUNT; x++) {
            for (int y = 0; y < TILE_HEIGHT_AMOUNT; y++) {
                Tile tile = new Tile(x, y);
                tile.addObserver(tileObserver);

                Bomb bomb = null;
                double chance = Math.random();

                if (chance < 0.1d)
                    bomb = new SmileBomb();
                else if (Math.random() < 0.2d)
                    bomb = new StandartBomb();

                if (bomb != null) {
                    bomb.addObserver(bombObserver);
                    tile.setBomb(bomb);
                }

                tiles[x][y] = tile;
            }
        }

        for (int x = 0; x < TILE_WIDTH_AMOUNT; x++) {
            for (int y = 0; y < TILE_HEIGHT_AMOUNT; y++) {
                Tile tile = tiles[x][y];

                if (!tile.hasBomb()) {
                    long bombs = getNeighborsOfTile(tile).stream().filter(t -> t.hasBomb()).count();

                    if (bombs > 0)
                        tile.setNeighborsBombCount(bombs);
                }
            }
        }
    }

    private List<sample.model.Tile> getNeighborsOfTile(Tile tile) {
        List<sample.model.Tile> result = new ArrayList<>();

        for (int i = 0; i < NEIGHBORS_POINTS.length; i++) {
            int newX = NEIGHBORS_POINTS[i][0] + tile.getX();
            int newY = NEIGHBORS_POINTS[i][1] + tile.getY();

            if (newX >= 0 && newX < TILE_WIDTH_AMOUNT && newY >= 0 && newY < TILE_HEIGHT_AMOUNT)
                result.add(tiles[newX][newY]);
        }

        return result;
    }

    public Parent getSceneParent() {
        return fieldView;
    }

    private Tile.Observer tileObserver = new Tile.Observer() {
        @Override
        public void tileOpened(Tile tile) {
            if (!tile.hasBomb() && !tile.hasNeighborsBomb())
                getNeighborsOfTile(tile).forEach(t -> t.open());
        }
    };

    private Bomb.Observer bombObserver = new Bomb.Observer() {
        @Override
        public void bombOpened(Bomb bomb) {
        }

        @Override
        public void bombDetonated(Bomb bomb) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Game Over!");
            alert.setHeaderText("Вы проиграли!");

            alert.setResultConverter(new Callback<ButtonType, ButtonType>() {
                @Override
                public ButtonType call(ButtonType param) {
                    if (param == ButtonType.OK) {
                        endGame();
                        startGame();
                    }

                    return null;
                }
            });

            alert.show();
        }
    };

}
