package sample.model;

import sample.view.BombView;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ultra on 14.07.2016.
 */
public abstract class Bomb {

    protected Observers observers = new Observers();

    public void detonate() {
        observers.notifyBombOpened(this);
        observers.notifyBombDetonated(this);
    }

    public abstract BombView getView(double width, double height);

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    protected class Observers<T extends Observer> extends ArrayList<T> {
        public void notifyBombOpened(Bomb bomb) {
            for (Iterator<T> iter = (Iterator<T>) iterator(); iter.hasNext();)
                iter.next().bombOpened(bomb);
        }

        public void notifyBombDetonated(Bomb bomb) {
            for (Iterator<T> iter = (Iterator<T>) iterator(); iter.hasNext();)
                iter.next().bombDetonated(bomb);
        }
    }


    public interface Observer {
        void bombOpened(Bomb bomb);
        void bombDetonated(Bomb bomb);
    }
}
