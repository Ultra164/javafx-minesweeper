package sample.model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ultra on 14.07.2016.
 */
public class Tile {

    private int x, y;
    private boolean isOpen = false;
    private Bomb bomb;
    private long neighborsBombCount = 0;

    private Observers observers = new Observers();

    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void open() {
        if (!isOpened()) {
            isOpen = true;

            if (hasBomb())
                bomb.detonate();
            else
                observers.notifyTileOpened(this);
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Bomb getBomb() {
        return bomb;
    }

    public long getNeighborsBombCount() {
        return neighborsBombCount;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void setNeighborsBombCount(long count) {
        neighborsBombCount = count;
    }

    public boolean hasBomb() {
        return bomb != null;
    }

    public boolean hasNeighborsBomb() {
        return neighborsBombCount > 0;
    }

    public boolean isOpened() {
        return isOpen;
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    private class Observers<T extends Observer> extends ArrayList<T> {
        public void notifyTileOpened(Tile tile) {
            for (Iterator<T> iter = (Iterator<T>) iterator(); iter.hasNext();)
                iter.next().tileOpened(tile);
        }
    }

    public interface Observer {
        void tileOpened(Tile tile);
    }
}
