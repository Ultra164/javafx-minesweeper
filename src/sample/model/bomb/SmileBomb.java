package sample.model.bomb;

import sample.model.Bomb;
import sample.view.BombView;
import sample.view.bomb.SmileBombView;
import sample.view.bomb.StandartBombView;

/**
 * Created by ultra on 14.07.2016.
 */
public class SmileBomb extends Bomb {

    @Override
    public void detonate() {
        observers.notifyBombOpened(this);
    }

    @Override
    public BombView getView(double width, double height) {
        return new SmileBombView(this, width, height);
    }

}
