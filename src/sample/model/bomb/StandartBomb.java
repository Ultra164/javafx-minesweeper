package sample.model.bomb;

import sample.model.Bomb;
import sample.view.BombView;
import sample.view.bomb.StandartBombView;

/**
 * Created by ultra on 14.07.2016.
 */
public class StandartBomb extends Bomb {

    @Override
    public void detonate() {
        super.detonate();
    }

    @Override
    public BombView getView(double width, double height) {
        return new StandartBombView(this, width, height);
    }

}
