package sample.view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.model.Bomb;

/**
 * Created by ultra on 14.07.2016.
 */
public abstract class BombView extends ImageView {

    private Bomb model;
    private double width, height;
    private String imagePath;

    public BombView(Bomb bomb, double width, double height, String imagePath) {
        this.model = bomb;
        this.width = width;
        this.height = height;
        this.imagePath = imagePath;

        initView();
    }

    private void initView() {
        setFitWidth(width);
        setFitHeight(height);

        Image image = getImageField();
        if (image == null) {
            image = new Image(imagePath);
            setImageField(image);
        }

        setImage(image);
        setVisible(false);
    }

    protected abstract Image getImageField();
    protected abstract void setImageField(Image image);
}
