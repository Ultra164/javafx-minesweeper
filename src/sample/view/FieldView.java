package sample.view;

import javafx.scene.layout.Pane;
import sample.model.Tile;

/**
 * Created by ultra on 14.07.2016.
 */
public class FieldView extends Pane {

    private double width, height;

    private TileView[][] tileViews;

    public FieldView(double width, double height) {
        this.width = width;
        this.height = height;

        setPrefSize(width, height);
    }

    public void initView(Tile[][] tiles) {
        tileViews = new TileView[tiles.length][tiles[0].length];

        for (int x = 0; x < tiles.length; x++) {
            for (int y = 0; y < tiles[0].length; y++) {
                Tile tile = tiles[x][y];
                TileView tileView = new TileView(tile, width / tiles.length, height / tiles[0].length);

                tileViews[x][y] = tileView;

                getChildren().add(tileView);
            }
        }
    }

    public void destroy() {
        getChildren().removeAll();

        for (int x = 0; x < tileViews.length; x++) {
            for (int y = 0; y < tileViews[0].length; y++) {
                TileView tileView = tileViews[x][y];

                tileView.destroy();
            }
        }
    }

}
