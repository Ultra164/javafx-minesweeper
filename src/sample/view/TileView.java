package sample.view;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import sample.model.Bomb;
import sample.model.Tile;

/**
 * Created by ultra on 14.07.2016.
 */
public class TileView extends StackPane {

    private static final int BORDER_SIZE = 2;

    private Tile tile;
    private double width, height;
    private Rectangle border;
    private Text text = new Text();
    private BombView bombView;

    public TileView(Tile tile, double width, double height) {
        this.tile = tile;
        this.width = width;
        this.height = height;

        initView();

        tile.addObserver(tileObserver);

        Bomb bomb = tile.getBomb();
        if (bomb != null)
            tile.getBomb().addObserver(bombObserver);
    }

    public void destroy() {
        tile.removeObserver(tileObserver);
        if (tile.hasBomb())
            tile.getBomb().removeObserver(bombObserver);
    }

    private void initView() {
        border = new Rectangle(width - BORDER_SIZE, height - BORDER_SIZE);
        border.setStroke(Color.LIGHTGRAY);
        border.setFill(Color.GRAY);

        text.setFont(Font.font(18));
        text.setVisible(false);

        getChildren().add(border);
        getChildren().add(text);

        if (tile.hasBomb()) {
            bombView = tile.getBomb().getView(width - BORDER_SIZE, height - BORDER_SIZE);
            getChildren().add(bombView);
        }

        setTranslateX(tile.getX() * width);
        setTranslateY(tile.getY() * height);

        setOnMouseClicked(e -> tile.open());
    }

    private Tile.Observer tileObserver = new Tile.Observer() {
        @Override
        public void tileOpened(Tile tile) {
            border.setFill(Color.WHITE);

            if (tile.hasNeighborsBomb())
                text.setText(String.valueOf(tile.getNeighborsBombCount()));

            text.setVisible(true);
        }
    };

    private Bomb.Observer bombObserver = new Bomb.Observer() {
        @Override
        public void bombOpened(Bomb bomb) {
            border.setFill(Color.WHITE);
            bombView.setVisible(true);
        }

        @Override
        public void bombDetonated(Bomb bomb) {
        }
    };
}
