package sample.view.bomb;

import javafx.scene.image.Image;
import sample.model.Bomb;
import sample.view.BombView;

/**
 * Created by ultra on 14.07.2016.
 */
public class SmileBombView extends BombView {

    private static Image image;

    public SmileBombView(Bomb bomb, double width, double height) {
        super(bomb, width, height, "smilebomb.png");
    }

    @Override
    protected Image getImageField() {
        return image;
    }

    @Override
    protected void setImageField(Image image) {
        SmileBombView.image = image;
    }
}
