package sample.view.bomb;

import javafx.scene.image.Image;
import sample.model.Bomb;
import sample.view.BombView;

/**
 * Created by ultra on 14.07.2016.
 */
public class StandartBombView extends BombView {

    private static Image image;

    public StandartBombView(Bomb bomb, double width, double height) {
        super(bomb, width, height, "standartbomb.png");
    }

    @Override
    protected Image getImageField() {
        return image;
    }

    @Override
    protected void setImageField(Image image) {
        StandartBombView.image = image;
    }
}
